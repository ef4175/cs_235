#ifndef SEARCH_H
#define SEARCH_H
#include "Node.h"

template<class T>
class Search {
private:
    Node<T> *root;
    void searchInternal(Node<T>*, int);
    void inorderInternal(Node<T>*);
public:
    Search();
    int getHeightInternal(Node<T>*);
    int getHeight();
    void search(int);
    Node<T>*& getRoot();
    virtual void insert(int, T) = 0;
    virtual void remove(int) = 0;
    void inorder();
    ~Search();
};

#include "Search.cpp"
#endif
