#ifndef AVL_H
#define AVL_H
#include "Node.h"
#include "Search.h"

template<class T>
class AVL : public Search<T> {
private:
    void insertInternal(Node<T>*&, Node<T>*);
    void removeInternal(Node<T>*&, int);
    Node<T>* popMaxOnLeft(Node<T>*&);
    void clearTree(Node<T>*&);
public:
    AVL();
    void insert(int, T);
    void remove(int);
    ~AVL();
};

#include "AVL.cpp"
#endif
