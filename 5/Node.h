#ifndef NODE_H
#define NODE_H

template<class T>
class Node{
private:
    int key;
    T value;
    Node<T> *left;
    Node<T> *right;
public:
    Node();
    Node(int, T);
    int getKey();
    T getValue();
    Node<T>*& getLeft();
    Node<T>*& getRight();
    void setKey(int);
    void setValue(T);
    void setLeft(Node<T>*);
    void setRight(Node<T>*);
    ~Node();
};

#include "Node.cpp"
#endif
