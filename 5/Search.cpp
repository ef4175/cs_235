template<class T>
Search<T>::Search() : root(NULL) {}

template<class T>
int Search<T>::getHeight() { return getHeightInternal(root); }

template<class T>
int Search<T>::getHeightInternal(Node<T>* n) {
    if (n == NULL) return -1;
    int l = getHeightInternal(n->getLeft());
    int r = getHeightInternal(n->getRight());
    if (l > r) return l + 1;
    return r + 1;
}

template<class T>
void Search<T>::search(int k) { searchInternal(root, k); }

template<class T>
void Search<T>::searchInternal(Node<T>* n, int k) {
    if (n == NULL) {
        std::cout << "Not found" << std::endl; return;
    } else if (n->getKey() == k) { //found
        std::cout << n->getValue() << std::endl; return;
    } else if (n->getKey() > k) { //go left
        searchInternal(n->getLeft(), k);
    } else if (n->getKey() < k) { //go right
        searchInternal(n->getRight(), k);
    }
}

template<class T>
void Search<T>::inorder() { inorderInternal(root); std::cout << std::endl; }

template<class T>
void Search<T>::inorderInternal(Node<T>* n) {
    if (n == NULL) return;
    inorderInternal(n->getLeft());
    std::cout << n->getValue() << " ";
    inorderInternal(n->getRight());
}

template<class T>
Node<T>*& Search<T>::getRoot() { return root; }

template<class T>
Search<T>::~Search() {}
