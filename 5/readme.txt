BST and AVL trees 

Input and output files shortened to 100,000 lines.

Best times after several attempts:
    |    By ID  |  By Birthday
-----------------------------------
BST |     240s  |    164s
AVL |     186s  |    191s        