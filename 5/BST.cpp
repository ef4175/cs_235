template<class T>
BST<T>::BST() : Search<T>::Search() {}

template<class T>
void BST<T>::insert(int k, T v) {
    Node<T> *n = new Node<T>(k, v);
    insertInternal(Search<T>::getRoot(), n);
}

template<class T>
void BST<T>::insertInternal(Node<T>*& r, Node<T>* n) {
    if (r == NULL) r = n; //found empty spot
    else if (r->getKey() > n->getKey())  //go left
        insertInternal(r->getLeft(), n);
    else if (r->getKey() < n->getKey())  //go right
        insertInternal(r->getRight(), n);
}

template<class T>
void BST<T>::remove(int k) { removeInternal(Search<T>::getRoot(), k); }

template<class T>
void BST<T>::removeInternal(Node<T>*& r, int k) {
    if (r == NULL) return;
    else if (r->getKey() > k)
        removeInternal(r->getLeft(), k);
    else if (r->getKey() < k)
        removeInternal(r->getRight(), k);
    else if (r->getKey() == k) {
        if (r->getLeft() == NULL && r->getRight() == NULL) { //delete root
            delete r; r = NULL;
        } else if (r->getLeft() == NULL && r->getRight() != NULL) { //delete
            Node<T> *temp = r->getRight();                          //left
            delete r;
            r = temp;
        } else if (r->getRight() == NULL && r->getLeft() != NULL) { //delete
            Node<T> *temp = r->getLeft();                           //right
            delete r;
            r = temp;
        } else { //pop max from left and use as new node
            Node<T> *maxOnLeft = popMaxOnLeft(r->getLeft());
            r->setKey(maxOnLeft->getKey());
            r->setValue(maxOnLeft->getValue());
        }
    }
}

template<class T>
Node<T>* BST<T>::popMaxOnLeft(Node<T>*& n) {
    while (n->getRight() != NULL) //keep going right
        n = n->getRight();
    Node<T> *temp = n;
    n = n->getRight();
    return temp;
}

template<class T>
void BST<T>::clearTree(Node<T>*& n) {
    if (n == NULL) return;
    clearTree(n->getLeft());
    clearTree(n->getRight());
    delete n; n = NULL;
}

template<class T>
BST<T>::~BST() { clearTree(Search<T>::getRoot()); }
