template<class T>
Node<T>::Node() : left(NULL), right(NULL) {}

template<class T>
Node<T>::Node(int k, T v) : key(k), value(v), left(NULL), right(NULL) {}

template<class T>
int Node<T>::getKey() { return key; }

template<class T>
T Node<T>::getValue() { return value; }

template<class T>
Node<T>*& Node<T>::getLeft() { return left; }

template<class T>
Node<T>*& Node<T>::getRight() { return right; }

template<class T>
void Node<T>::setKey(int k) { key = k; }

template<class T>
void Node<T>::setValue(T v) { value = v; }

template<class T>
void Node<T>::setLeft(Node<T>* n) { left = n; }

template<class T>
void Node<T>::setRight(Node<T>* n) { right = n; }

template<class T>
Node<T>::~Node() {}
