template<class T>
AVL<T>::AVL() : Search<T>() {}

template<class T>
void AVL<T>::insert(int k, T v) {
    Node<T> *n = new Node<T>(k, v);
    insertInternal(Search<T>::getRoot(), n);
}

template<class T>
void AVL<T>::insertInternal(Node<T>*& r, Node<T>* n) {
    if (r == NULL) r = n; //found empty spot
    else if (r->getKey() > n->getKey()) {  //go left
        insertInternal(r->getLeft(), n);
        if (Search<T>::getHeightInternal(r->getLeft()) //imbalance on left
            -Search<T>::getHeightInternal(r->getRight()) == 2) {
            Node<T> *left = r->getLeft();
            Node<T> *leftLeft = left->getLeft();
            Node<T> *leftRight = left->getRight();
            Node<T> *temp = r;
            if (Search<T>::getHeightInternal(r->getLeft()->getLeft())
            >Search<T>::getHeightInternal(r->getLeft()->getRight())) { //L-L
                r = left;
                r->setRight(temp);
                temp->setLeft(leftRight);
            } else {  //L-R
                Node<T> *leftRightLeft = leftRight->getLeft();
                Node<T> *leftRightRight = leftRight->getRight();
                left->setRight(leftRightLeft);
                r = leftRight;
                r->setLeft(left);
                r->setRight(temp);
                temp->setLeft(leftRightRight);
            }
        }
    }
    else if (r->getKey() < n->getKey()) { //go right
        insertInternal(r->getRight(), n);
        if (Search<T>::getHeightInternal(r->getRight()) //inbalance on right
            -Search<T>::getHeightInternal(r->getLeft()) == 2) {
            Node<T> *right = r->getRight();
            Node<T> *rightRight = right->getRight();
            Node<T> *rightLeft = right->getLeft();
            Node<T> *temp = r;
            if (Search<T>::getHeightInternal(r->getRight()->getRight()) 
            >Search<T>::getHeightInternal(r->getRight()->getLeft())) { //R-R
                r = right;
                r->setLeft(temp);
                temp->setRight(rightLeft);
            } else {  //R-L
                Node<T> *rightLeftRight = rightLeft->getRight();
                Node<T> *rightLeftLeft = rightLeft->getLeft();
                right->setLeft(rightLeftRight);
                r = rightLeft;
                r->setRight(right);
                r->setLeft(temp);
                temp->setRight(rightLeftLeft);
            }
        }
    }
}

template<class T>
void AVL<T>::remove(int k) { removeInternal(Search<T>::getRoot(), k); }

template<class T>
void AVL<T>::removeInternal(Node<T>*& r, int k) {
    if (r == NULL) return;
    else if (r->getKey() > k)
        removeInternal(r->getLeft(), k);
    else if (r->getKey() < k)
        removeInternal(r->getRight(), k);
    else if (r->getKey() == k) {
        if (r->getLeft() == NULL && r->getRight() == NULL) { //delete root
            delete r; r = NULL;
        } else if (r->getLeft() == NULL && r->getRight() != NULL) { //delete
            Node<T> *temp = r->getRight();                          //left
            delete r;
            r = temp;
        } else if (r->getRight() == NULL && r->getLeft() != NULL) { //delete
            Node<T> *temp = r->getLeft();                           //right
            delete r;
            r = temp;
        } else { //pop max from left and use as new node
            Node<T> *maxOnLeft = popMaxOnLeft(r->getLeft());
            r->setKey(maxOnLeft->getKey());
            r->setValue(maxOnLeft->getValue());
        }
    }
}

template<class T>
Node<T>* AVL<T>::popMaxOnLeft(Node<T>*& n) {
    while (n->getRight() != NULL) //keep going right
        n = n->getRight();
    Node<T> *temp = n;
    n = n->getRight();
    return temp;
}

template<class T>
void AVL<T>::clearTree(Node<T>*& n) {
    if (n == NULL) return;
    clearTree(n->getLeft());
    clearTree(n->getRight());
    delete n; n = NULL;
}

template<class T>
AVL<T>::~AVL() { clearTree(Search<T>::getRoot()); }