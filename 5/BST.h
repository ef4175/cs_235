#ifndef BST_H
#define BST_H
#include "Node.h"
#include "Search.h"

template<class T>
class BST : public Search<T> {
private:
    void insertInternal(Node<T>*&, Node<T>*);
    void removeInternal(Node<T>*&, int);
    Node<T>* popMaxOnLeft(Node<T>*&);
    void clearTree(Node<T>*&);
public:
    BST();
    void insert(int, T);
    void remove(int);
    ~BST();
};

#include "BST.cpp"
#endif
