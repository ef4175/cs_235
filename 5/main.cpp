#include <iostream>
#include <fstream>
#include <string>
#include <cstdlib>
#include <ctime>
#include "Search.h"
#include "BST.h"
#include "AVL.h"

int strToInt(std::string);
int parseLine(std::string, int);

int main(int argc, char* argv[]) {

    Search<int> *search;
    BST<int> *bstID = new BST<int>();
    BST<int> *bstBD = new BST<int>();
    AVL<int> *avlID = new AVL<int>();
    AVL<int> *avlBD = new AVL<int>();
    std::string line;
    int id, year, month, day;
    
    search = bstID;
    std::ifstream input1("input.csv");
    std::clock_t startBST1 = std::clock();
    while (input1.good()) {
        std::getline(input1, line, '\n');
        if (line.length()) {
            id = parseLine(line, 0);
            month = parseLine(line, 1);
            day = parseLine(line, 2);
            year = parseLine(line, 3);
            search->insert(id, year*10000 + month*100 + day);
        } 
    }
    std::clock_t endBST1 = std::clock();
    input1.close();
    std::cout << "BST took " 
              << (endBST1-startBST1)/(double)(CLOCKS_PER_SEC/1000)
              << " milliseconds to insert by ID" << std::endl;
    
    
    search = avlID;
    std::ifstream input2("input.csv");
    std::clock_t startAVL1 = std::clock();
    while (input2.good()) {
        std::getline(input2, line, '\n');
        if (line.length()) {
            id = parseLine(line, 0);
            month = parseLine(line, 1);
            day = parseLine(line, 2);
            year = parseLine(line, 3);
            search->insert(id, year*10000 + month*100 + day);
        }
    }
    std::clock_t endAVL1 = std::clock();
    input2.close();
    std::cout << "AVL took " 
              << (endAVL1-startAVL1)/(double)(CLOCKS_PER_SEC/1000)
              << " milliseconds to insert by ID" << std::endl;


    search = bstBD;
    std::ifstream output1("output.csv");
    std::clock_t startBST2 = std::clock();
    while (output1.good()) {
        std::getline(output1, line, '\n');
        if (line.length()) {
            id = parseLine(line, 0);
            month = parseLine(line, 1);
            day = parseLine(line, 2);
            year = parseLine(line, 3);
            search->insert(year*10000 + month*100 + day, id);
        }
    }
    std::clock_t endBST2 = std::clock();
    output1.close();
    std::cout << "BST took "
              << (endBST2-startBST2)/(double)(CLOCKS_PER_SEC/1000)
              << " milliseconds to insert by birthday" << std::endl;
    
    
    search = avlBD;
    std::ifstream output2("output.csv");
    std::clock_t startAVL2 = std::clock();
    while (output2.good()) {
        std::getline(output2, line, '\n');
        if (line.length()) {
            id = parseLine(line, 0);
            month = parseLine(line, 1);
            day = parseLine(line, 2);
            year = parseLine(line, 3);
            search->insert(year*10000 + month*100 + day, id);
        }
    }
    std::clock_t endAVL2 = std::clock();
    output2.close();
    std::cout << "AVL took "
              << (endAVL2-startAVL2)/(double)(CLOCKS_PER_SEC/1000)
              << " milliseconds to insert by birthday" << std::endl;
              
    
    delete bstID; bstID = NULL;
    delete avlID; avlID = NULL;
    delete bstBD; bstBD = NULL;
    delete search; avlBD = NULL; search = NULL;          
              
    return 0;
}

int strToInt(std::string s) {
    int result = 0;
    int power = 1;
    while (s.length() > 0) {
        switch (s[s.length() - 1]) {
            case '1': result += power; break;
            case '2': result += power*2; break;
            case '3': result += power*3; break;
            case '4': result += power*4; break;
            case '5': result += power*5; break;
            case '6': result += power*6; break;
            case '7': result += power*7; break;
            case '8': result += power*8; break;
            case '9': result += power*9; break;
            default: break;
        }
        power *= 10;
        s = s.substr(0, s.length()-1);
    }
    return result;
}

int parseLine(std::string line, int i) {
    int j = 0, k = 0, l = 0;
    int commas = 0;
    switch (i) {
        case 0: while (line[j] != ',')
                    j++;
                return strToInt(line.substr(0, j));
        case 1: while (commas != 1) {
                    if (line[j] == ',')
                        commas++;
                    j++;
                }
                k = j;
                while (line[k] != ',') { k++; l++; }
                return strToInt(line.substr(j, l));
        case 2: while (commas != 2) {
                    if (line[j] == ',')
                        commas++;
                    j++;
                }
                k = j;
                while (line[k] != ',') { k++; l++; }
                return strToInt(line.substr(j, l));
        case 3: j = line.length() - 1;
                while (line[j] != ',')
                    j--;
                return strToInt(line.substr(j, line.length()-j));
        default: return 0;
    }
}
