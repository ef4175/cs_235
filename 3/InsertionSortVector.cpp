#include <iostream>
#include <vector>
#include "BankAccount.h"

template <class T>
void insertionSort(std::vector<T>&);

int main(int argc, char* argv[]) {

    std::vector<int> foo;
    foo.push_back(5);
    foo.push_back(8);
    foo.push_back(1);
    foo.push_back(2);
    foo.push_back(4);
    foo.push_back(7);
    foo.push_back(6);
    foo.push_back(3);
    for (int i = 0; i < foo.size(); i++)
        std::cout << foo[i] << " ";
    std::cout << std::endl;
    insertionSort(foo);
    for (int i = 0; i < foo.size(); i++)
        std::cout << foo[i] << " ";
    std::cout << std::endl << std::endl;
    
    
    std::vector<double> bar;
    bar.push_back(5.5);
    bar.push_back(8.8);
    bar.push_back(1.1);
    bar.push_back(2.2);
    bar.push_back(4.4);
    bar.push_back(7.7);
    bar.push_back(6.6);
    bar.push_back(3.3);
    for (int i = 0; i < bar.size(); i++)
        std::cout << bar[i] << " ";
    std::cout << std::endl;
    insertionSort(bar);
    for (int i = 0; i < bar.size(); i++)
        std::cout << bar[i] << " ";
    std::cout << std::endl << std::endl;
    
    
    std::vector<BankAccount> men(8);
    men[0].setBalance(420); men[0].setInterest(0.1);
    men[1].setBalance(300); men[1].setInterest(0.08);
    men[2].setBalance(1920); men[2].setInterest(0.16);
    men[3].setBalance(666); men[3].setInterest(0.18);
    men[4].setBalance(1080); men[4].setInterest(0.03);
    men[5].setBalance(0); men[5].setInterest(1.2);
    men[6].setBalance(2390); men[6].setInterest(0.75);
    men[7].setBalance(515); men[7].setInterest(1);
    for (int i = 0; i < men.size(); i++)
        std::cout << men[i].getBalance() << " ";
    std::cout << std::endl;
    insertionSort(men);
    for (int i = 0; i < men.size(); i++)
        std::cout << men[i].getBalance() << " ";
    std::cout << std::endl;
    
    return 0;
}

template <class T>
void insertionSort(std::vector<T>& vec) {
    int size = vec.size();
    for (int i = 1; i < size; i++) {
        int j = i - 1;
        while (j >= 0 && vec[j + 1] < vec[j]) {
            T temp = vec[j + 1];
            vec[j + 1] = vec[j];
            vec[j--] = temp;
        }
    }
}
