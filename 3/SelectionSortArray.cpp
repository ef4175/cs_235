#include <iostream>
#include "BankAccount.h"

template <class T>
void selectionSort(T[], int);

int main(int argc, char* argv[]) {

    const int SIZE = 8;
    
    int foo[SIZE] = {5, 8, 1, 2, 4, 7, 6, 3};
    for (int i = 0; i < SIZE; i++)
        std::cout << foo[i] << " ";
    std::cout << std::endl;
    selectionSort(foo, SIZE);
    for (int i = 0; i < SIZE; i++)
        std::cout << foo[i] << " ";
    std::cout << std::endl << std::endl;
    
    
    double bar[SIZE] = {5.5, 8.8, 1.1, 2.2, 4.4, 7.7, 6.6, 3.3};
    for (int i = 0; i < SIZE; i++)
        std::cout << bar[i] << " ";
    std::cout << std::endl;
    selectionSort(bar, SIZE);
    for (int i = 0; i < SIZE; i++)
        std::cout << bar[i] << " ";
    std::cout << std::endl << std::endl;
    
    
    BankAccount men[SIZE];
    men[0].setBalance(420); men[0].setInterest(0.1);
    men[1].setBalance(300); men[1].setInterest(0.08);
    men[2].setBalance(1920); men[2].setInterest(0.16);
    men[3].setBalance(666); men[3].setInterest(0.18);
    men[4].setBalance(1080); men[4].setInterest(0.03);
    men[5].setBalance(0); men[5].setInterest(1.2);
    men[6].setBalance(2390); men[6].setInterest(0.75);
    men[7].setBalance(515); men[7].setInterest(1);
    for (int i = 0; i < SIZE; i++)
        std::cout << men[i].getBalance() << " ";
    std::cout << std::endl;
    selectionSort(men, SIZE);
    for (int i = 0; i < SIZE; i++)
        std::cout << men[i].getBalance() << " ";
    std::cout << std::endl;

    return 0;
}

template <class T>
void selectionSort(T arr[], int size) {
    for (int i = 0; i < size; i++) {
        int indexSmallest = i;
        for (int j = i + 1; j < size; j++) {
            if (arr[j] < arr[indexSmallest])
                indexSmallest = j;
        }
        T temp = arr[i];
        arr[i] = arr[indexSmallest];
        arr[indexSmallest] = temp;
    }
}
