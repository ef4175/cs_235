Implementation of four sorting algorithms for third assignment,
using both arrays and vectors.

Some notes:
    In the array implementation, mergeSort and quickSort both are wrappers
    for their actual sorting algorithms. This visually improves APi, i.e. 
    it is consistent with other functions that take just two arguments:
    an array and the size of that array. Conversely, in the vector 
    implementation, these wrappers take just one argument: an address of a 
    vector.
    
    Seperate merging and partitioning for mergeSort and quickSort 
    respectively were not written. To save some overhead, these operations 
    were written directly in the sorting algorithms.
    
    A simple BankAccount class with overloaded comparison operators
    was created to demonstrate sorting using template functions.
