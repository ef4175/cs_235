#ifndef BANKACCOUNT_H
#define BANKACCOUNT_H

class BankAccount {
private:
    double balance;
    double interest;
public:
    BankAccount(double b = 0, double i = 0): balance(b), interest(i) {}
    void setBalance(double b) { balance = b; }
    void setInterest(double i) { interest = i; }
    double getBalance() { return balance; }
    double getInterest() { return interest; }
    bool operator >(const BankAccount& b) {
        return this->balance > b.balance;
    }
    bool operator <(const BankAccount& b) {
        return this->balance < b.balance;
    }
    bool operator <=(const BankAccount& b) {
        return this->balance <= b.balance;
    }
    bool operator >=(const BankAccount& b) {
        return this->balance >= b.balance;
    }
    ~BankAccount() {}
};

#endif
