#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <cstdlib>
#include <ctime>

const int NUM_STUDENTS = 1000000;

class Student {
public:
    int id;
    int year;
    int month;
    int day;
    int totalDays;
    
    Student(int i=0, int y=0, int m=0, int d=0, int t=0):
        id(i), year(y), month(m), day(d), totalDays(t) {}
    
    ~Student() {}
};

/* File generator */
void makeFile();

/* Functions for file io */
int strToInt(std::string);
int parseLine(std::string, int);

/* Helper functions for count and radix sorts */
int getMax(std::vector<Student>&, int sortID=0);
int getNumDigits(int);

/* Count sort  */
void countSort(std::vector<Student>&, int sortID=0);

/* Radix sort */
void radixCountSort(std::vector<Student>&, int, int sortID=0);
void radixSort(std::vector<Student>&, int sortID=0);

/* Heap sort */
void pushDown(std::vector<Student>&, int, int sortID=0);
void buildMaxHeap(std::vector<Student>&, int sortID=0);
void heapSort(std::vector<Student>&, int sortID=0);

int main(int argc, char* argv[]) {

    srand(time(0));
    makeFile();
    
    std::ifstream input("input.csv");
    std::string line;
    std::vector<Student> studentsDOBCount;
    std::vector<Student> studentsDOBRadix;
    std::vector<Student> studentsDOBHeap;
    
    int id, year, month, day, totalDays;

    /* Create vectors of students using data from input.csv */
    while (input.good()) {
        std::getline(input, line, '\n');
        if (line.length()) {
            id = parseLine(line, 0);
            month = parseLine(line, 1);
            day = parseLine(line, 2);
            year = parseLine(line, 3);
            Student s(id, year, month, day, 365*year + 30*month + day);
            studentsDOBCount.push_back(s);
            studentsDOBRadix.push_back(s);
            studentsDOBHeap.push_back(s);
        }
    }
    
    std::clock_t startCS1 = std::clock();
    countSort(studentsDOBCount);
    std::clock_t endCS1 = std::clock();
    std::cout << "Count sort took "
              << (endCS1-startCS1)/(double)(CLOCKS_PER_SEC/1000)
              << " milliseconds to sort by birthday" << std::endl;
    
    std::clock_t startRS1 = std::clock();
    radixSort(studentsDOBRadix);
    std::clock_t endRS1 = std::clock();
    std::cout << "Radix sort took "
              << (endRS1-startRS1)/(double)(CLOCKS_PER_SEC/1000)
              << " milliseconds to sort by birthday" << std::endl;
    
    std::clock_t startHS1 = std::clock();
    heapSort(studentsDOBHeap);
    std::clock_t endHS1 = std::clock();
    std::cout << "Heap sort took "
              << (endHS1-startHS1)/(double)(CLOCKS_PER_SEC/1000) 
              << " milliseconds to sort by birthday" << std::endl;
    
    std::ofstream output1("output_count_sort_birthday.csv");
    for (int i = 0; i < studentsDOBCount.size(); i++)
        output1 << studentsDOBCount[i].id << ',' << studentsDOBCount[i].month
                << ',' << studentsDOBCount[i].day << ','
                << studentsDOBCount[i].year << '\n';
    output1.close();
    
    std::ofstream output2("output_radix_sort_birthday.csv");
    for (int i = 0; i < studentsDOBRadix.size(); i++)  
        output2 << studentsDOBRadix[i].id << ',' << studentsDOBRadix[i].month
                << ',' << studentsDOBRadix[i].day << ','
                << studentsDOBRadix[i].year << '\n';
    output2.close();
        
    std::ofstream output3("output_heap_sort_birthday.csv");
    for (int i = 0; i < studentsDOBHeap.size(); i++)  
        output3 << studentsDOBHeap[i].id << ',' << studentsDOBHeap[i].month
                << ',' << studentsDOBHeap[i].day << ','
                << studentsDOBHeap[i].year << '\n';
    output3.close();
    
    input.close();
    
    
    
    std::ifstream output("output_heap_sort_birthday.csv");
    std::vector<Student> studentsIDCount;
    std::vector<Student> studentsIDRadix;
    std::vector<Student> studentsIDHeap;
    
    while (output.good()) {
        std::getline(output, line, '\n');
        if (line.length()) {
            id = parseLine(line, 0);
            month = parseLine(line, 1);
            day = parseLine(line, 2);
            year = parseLine(line, 3);
            Student s(id, year, month, day, 365*year + 30*month + day);
            studentsIDCount.push_back(s);
            studentsIDRadix.push_back(s);
            studentsIDHeap.push_back(s);
        }
    }
    
    std::clock_t startCS2 = std::clock();
    countSort(studentsIDCount, 1);
    std::clock_t endCS2 = std::clock();
    std::cout << "Count sort took " 
              << (endCS2-startCS2)/(double)(CLOCKS_PER_SEC/1000) 
              << " milliseconds to sort by id" << std::endl;
    
    std::clock_t startRS2 = std::clock();
    radixSort(studentsIDRadix, 1);
    std::clock_t endRS2 = std::clock();
    std::cout << "Radix sort took "
              << (endRS2-startRS2)/(double)(CLOCKS_PER_SEC/1000) 
              << " milliseconds to sort by id" << std::endl;
    
    std::clock_t startHS2 = std::clock();         
    heapSort(studentsIDHeap, 1);
    std::clock_t endHS2 = std::clock();
    std::cout << "Heap sort took "
              << (endHS2-startHS2)/(double)(CLOCKS_PER_SEC/1000) 
              << " milliseconds to sort by id" << std::endl;
    
    std::ofstream output4("output_count_sort_id.csv");
    for (int i = 0; i < studentsIDCount.size(); i++)  
        output4 << studentsIDCount[i].id << ',' << studentsIDCount[i].month
                << ',' << studentsIDCount[i].day << ','
                << studentsIDCount[i].year << '\n';
    output4.close();
    
    std::ofstream output5("output_radix_sort_id.csv");
    for (int i = 0; i < studentsIDRadix.size(); i++)  
        output5 << studentsIDRadix[i].id << ',' << studentsIDRadix[i].month
                << ',' << studentsIDRadix[i].day << ','
                << studentsIDCount[i].year << '\n';
    output5.close();
        
    std::ofstream output6("output_heap_sort_id.csv");
    for (int i = 0; i < studentsIDHeap.size(); i++)  
        output6 << studentsIDHeap[i].id << ',' << studentsIDHeap[i].month
                << ',' << studentsIDHeap[i].day << ','
                << studentsIDCount[i].year << '\n';
    output6.close();
    
    output.close();
    
    
    return 0;
}

void makeFile() {
    std::ofstream file("input.csv");
    for (int i = 0; i < NUM_STUDENTS; i++) {
        file << i + 1 << ',';
        file << rand() % 12 + 1 << ',';
        file << rand() % 30 + 1 << ',';
        file << rand() % 501 + 1500 << '\n';
    }
    file.close();
}

int strToInt(std::string s) {
    int result = 0;
    int power = 1;
    while (s.length() > 0) {
        switch (s[s.length() - 1]) {
            case '1': result += power; break;
            case '2': result += power*2; break;
            case '3': result += power*3; break;
            case '4': result += power*4; break;
            case '5': result += power*5; break;
            case '6': result += power*6; break;
            case '7': result += power*7; break;
            case '8': result += power*8; break;
            case '9': result += power*9; break;
            default: break;
        }
        power *= 10;
        s = s.substr(0, s.length()-1);
    }
    return result;
}

/* Parse line in .csv to return int values */
int parseLine(std::string line, int i) {
    int j = 0, k = 0, l = 0;
    int commas = 0;
    switch (i) {
        case 0: while (line[j] != ',')
                    j++;
                return strToInt(line.substr(0, j));
        case 1: while (commas != 1) {
                    if (line[j] == ',')
                        commas++;
                    j++;
                }
                k = j;
                while (line[k] != ',') { k++; l++; }
                return strToInt(line.substr(j, l));
        case 2: while (commas != 2) {
                    if (line[j] == ',')
                        commas++;
                    j++;
                }
                k = j;
                while (line[k] != ',') { k++; l++; }
                return strToInt(line.substr(j, l));
        case 3: j = line.length() - 1;
                while (line[j] != ',')
                    j--;
                return strToInt(line.substr(j, line.length()-j));
        default: return 0;
    }
}

int getMax(std::vector<Student>& v, int sortID) {
    if (!sortID) {
        int max = v[0].totalDays;
        for (int i = 1; i < v.size(); i++)
            if (v[i].totalDays > max)
                max = v[i].totalDays;
        return max;
    } else {
        int max = v[0].id;
        for (int i = 1; i < v.size(); i++)
            if (v[i].id > max)
                max = v[i].id;
        return max;
    }
}

int getNumDigits(int n) {
    int counter = 1;
    while (n /= 10)
        counter++;
    return counter;
}

void countSort(std::vector<Student>& v, int sortID) {
    /* Create array of vector of students */
    int max = getMax(v, sortID);
    std::vector<Student> *temp = new std::vector<Student>[max+1];
    
    /* At array index totalDays or id of current student,
    append student to its vector */
    if (!sortID) {
        for (int i = 0; i < v.size(); i++)
            temp[v[i].totalDays].push_back(v[i]);
    } else {
        for (int i = 0; i < v.size(); i++)
            temp[v[i].id].push_back(v[i]);
    }
    
    /* Check all vectors in array. If vector is not empty,
    iterate through its elements and copy them into paramter
    vector */
    int k = 0;
        for (int i = 0; i <= max; i++) 
            if (temp[i].size() > 0) 
                for (int j = 0; j < temp[i].size(); j++) 
                    v[k++] = temp[i][j];
        
    delete[] temp;
}

void radixCountSort(std::vector<Student>& v, int power, int sortID) {
    std::vector<Student> digits[10];
    
    /* Put student in nth position of array, where n is a digit between
    0 and 9 inclusive */
    if (!sortID) {
        for (int i = 0; i < v.size(); i++)
            digits[(v[i].totalDays / power) % 10].push_back(v[i]);
    } else {
        for (int i = 0; i < v.size(); i++)
            digits[(v[i].id / power) % 10].push_back(v[i]);
    }
    
    int k = 0;
        for (int i = 0; i < 10; i++)
            if (digits[i].size() > 0)
                for (int j = 0; j < digits[i].size(); j++)
                    v[k++] = digits[i][j];
}

void radixSort(std::vector<Student>& v, int sortID) {
    /* Get number of digits of greatest totalDays */
    int mostDigits = getNumDigits(getMax(v, sortID));
    int power = 1;
    
    while (getNumDigits(power) <= mostDigits) {
        radixCountSort(v, power, sortID);
        power *= 10;
    }
}

void pushDown(std::vector<Student>& v, int i, int size, int sortID) {
    if (!sortID) {
        while (i < size) {
            if (2*i+1 >= size) {  //if leaf
                return;
            } else if (2*i+2 >= size) {  //if node has left child only
                if (v[i].totalDays < v[2*i + 1].totalDays) {
                    Student temp = v[i];
                    v[i] = v[2*i + 1];
                    v[2*i + 1] = temp;
                }
                return;
            } else if (v[i].totalDays < v[2*i+1].totalDays || 
                       v[i].totalDays < v[2*i+2].totalDays) {
                Student temp = v[i];
                if (v[2*i + 1].totalDays > v[2*i + 2].totalDays) {
                    v[i] = v[2*i + 1];
                    v[2*i + 1] = temp;
                    i = 2*i + 1;
                } else {
                    v[i] = v[2*i + 2];
                    v[2*i + 2] = temp;
                    i = 2*i + 2;
                }
            } else return;
        }
    } else {
        while (i < size) {
            if (2*i+1 >= size) {
                return;
            } else if (2*i+2 >= size) {
                if (v[i].id < v[2*i + 1].id) {
                    Student temp = v[i];
                    v[i] = v[2*i + 1];
                    v[2*i + 1] = temp;
                }
                return;
            } else if (v[i].id < v[2*i+1].id || v[i].id < v[2*i+2].id) {
                Student temp = v[i];
                if (v[2*i + 1].id > v[2*i + 2].id) {
                    v[i] = v[2*i + 1];
                    v[2*i + 1] = temp;
                    i = 2*i + 1;
                } else {
                    v[i] = v[2*i + 2];
                    v[2*i + 2] = temp;
                    i = 2*i + 2;
                }
            } else return;
        }
    }
}

void buildMaxHeap(std::vector<Student>& v, int sortID) {
    for (int i = v.size()/2; i >= 0; i--)
        pushDown(v, i, v.size(), sortID);
}

void heapSort(std::vector<Student>& v, int sortID) {
    buildMaxHeap(v, sortID);
    for (int i = 0; i < v.size()-1; i++) {
        Student temp = v[0];
        v[0] = v[v.size() - i - 1];
        v[v.size() - i - 1] = temp;
        pushDown(v, 0, v.size() - i - 1, sortID);
    }
}
