Sorting .csv file under certain criteria, using three sorting algorithms.

Input and output files are formatted as ID, month, day, year.

Sorting algorithms take a second optional argument, which, if not zero,
sorts data by ID rather than by DOB.

Best times after several tries on machine with 2GB RAM and 2.00GHz
dual-core Intel processor:

           | By birthday  |   By id
-------------------------------------------
Count sort |   764.045ms  |    886.953ms
Radix sort |  1054.120ms  |   1488.380ms
Heap sort  |  1815.830ms  |   1811.200ms
