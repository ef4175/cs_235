Connectivity problem for CS 235

Some remarks about WeightedQuickUnionHeight class:
	If tree containing A and tree containing B are of the same height, the root of the latter tree will point to that of former tree, and the former tree will have its height adjusted by one (lines 136 - 138).
	Examples:
	0  1   => 0 
	          |
	          1   Height of 0 goes from 1 to 2, i.e. += 1.
				
	0    2 => 0
	|    |   / \
	1    3  1   2
	            |
	            3  Height of 0 goes from 2 to 3, i.e. += 1.
					
	Heights of trees will not be adjusted if one is shorter than the other. Instead, the trees will just be united (line 134).
	Examples:
	0    2 =>  0
	|         / \
	1        1   2  Height of 0 remains 2.
	
	  0       4      =>     0 
	 / \     / \          / | \
	1   2   5   6        1  2  4
 	|                    |    / \
 	3                    3   5   6  Height of 0 remains 3.
 	  
 	  Consider line 213.
 	  Before the union operation, the trees would look like this:
      0    1  3
     / \   |  |
    5   2  4  6
 	  We want to unite 0 and 4.
 	  The result is:
      0      3
    / | \    |
   2  1  5   6
      |
      4
 	  The root of 4 points to the root of 0, and the height of 0 is incremented by one, after which it will be 3. This is the least worst result among the 3 QuickUnion classes because, had we made 0 point to 4, the result would be a tree such that 1 is the root and its height is 4.
 	    
4. Prove that WeightedQuickUnionHeight instances require at most lg(n) array accesses to determine whether two objects are connected.
	Two objects are connected if their roots are equal. Finding the root of any given node requires, in the worst case, n array accesses, where n is the height of the tree. 

	Let's prove that the height of a tree with n nodes is lg(n) by induction. 
		Base case: n = 0 => lg(0) = 1
		Suppose we have two trees of size a and b, where a < b, and assume that their respective heights are lg(a) and lg(b). If we append the smaller tree to the larger one and a + b = n, we effectively increase the height of the smaller tree by one because its root now has a parent and we create a tree with n nodes. The logarithmic property remains because lg(a) + 1 (obtained from the increased height) = lg(2a) <= lg(a + b) = lg(n), i.e. 2a will never exceed a + b.

5. Solve the recurrences:
	a. C(n) = C(n/2) + n^2 for n >= 2 with C(1) = 0 and n is a power of two.
		C(1)  =                0    
		C(2)  = C(1)  + 2^2  = 4    
		C(4)  = C(2)  + 4^2  = 20   
		C(8)  = C(4)  + 8^2  = 84   
  		C(16) = C(8)  + 16^2 = 340  
		C(32) = C(16) + 32^2 = 1364 
		C(64) = C(32) + 64^2 = 5460 
		.
		.
		.
		It seems that, for any given term, its value is 4 times the previous one + 4, and that the nth term of this sequence is (4/3)*(4^n - 1).
		Inductive proof:
			Base case: n = 0 => 0
			Next: n = 1 =>      4
			Next: n = 2 =>      20
			Assume that this property holds for all n < m
				(4/3)*(4^(m-1) - 1) * 4 + 4 =
				(16/3)*(4^(m-1) - 1) + 4 =
				(4^(m+1))/3 - 16/3 + 4 =
				(4^(m+1))/3 - 4/3 =
				(1/3)*((4^(m+1)) - 4) =
				(4/3)*(4^m - 1)

		O(n^2 + (n/2)^2) ~ O(n^2)
				
	b. C(n) = aC(n/2) for n >= 2 with C(1) = 1 and n is a power of two.
		C(1)  = 1     =         a^0
		C(2)  = aC(1) = a*1   = a
		C(4)  = aC(2) = a*a   = a^2
		C(8)  = aC(4) = a*a^2 = a^3
		C(16) = aC(8) = a*a^3 = a^4
		.
		.
		.

		The nth term of this sequence is a^n
		Inductive proof:
			Base case: n = 0
			Assume that this property holds for all n < m
			a^(m - 1) * a = 
			a^(m - 1 + 1) = 
			a^m

		O(n^(lg(n)))
			
	c. C(n) = C(n/a) + 1 for n >= 2 with C(1) = 0 and n is a power of a.
		C(1) =                                 0
		C(a) = C(a/a) + 1 = C(1) + 1         = 1
		C(a^2) = C(a^2 / a) + 1 = C(a) + 1   = 2
		C(a^3) = C(a^3 / a) + 1 = C(a^2) + 1 = 3
		C(a^4) = C(a^4 / a) + 1 = C(a^3) + 1 = 4
		.
		.
		.
		
		The nth term of this sequence is n.
		Inductive proof:
			Base case: n = 0 => 0
			Assume that this property holds for all n < m
			C(a^(m-1)) + 1 = m - 1
			C(a^m) + 1 = 
			C((a^m) / a) + 1 =
			C(a^(m-1)) + 1 =
			m - 1 + 1 =
			m

		O(log(n)), where the base is a