#include <iostream>

class QuickFind {
private:
	int *id;
	int size;
public:
	QuickFind() { size = 0; }
	QuickFind(int s) {
		size = s;
		id = new int[size];
		for (int n = 0; n < size; n++)
			id[n] = n;
	}
	int find(int a) { return id[a]; }
	void unite(int a, int b) { //All in group B now belong to group A. Choice is arbitrary.
		int idA = id[a];
		int idB = id[b];
		for (int n = 0; n < size; n++) {
			if (id[n] == idB)
				id[n] = idA;
		}
	}
	void print() {
		std::cout << "Index   ";
		for (int n = 0; n < size; n++)
			std::cout << n << " ";
		std::cout << std::endl << "Content ";
		for (int n = 0; n < size; n++)
			std::cout << id[n] << " ";
		std::cout << std::endl << std::endl;
	}
	~QuickFind() { delete[] id; }
};

class QuickUnion {
private:
	int *id;
	int size;
public:
	QuickUnion() { size = 0; }
	QuickUnion(int s) {
		size = s;
		id = new int[size];
		for (int n = 0; n < size; n++)
			id[n] = n;
	}
	int getRoot(int a) { //Traverse parents until root is found
		while (a != id[a])
			a = id[a];
		return a;
	}
	int find(int a) { return getRoot(a); }
	void unite(int a, int b) { //One root becomes child of another root. Choice is arbitrary in this case.
		id[a] = b;
	}
	void print() {
		std::cout << "Index   ";
		for (int n = 0; n < size; n++)
			std::cout << n << " ";
		std::cout << std::endl << "Content ";
		for (int n = 0; n < size; n++)
			std::cout << id[n] << " ";
		std::cout << std::endl << std::endl;
	}
	~QuickUnion() { delete[] id; }
};

class WeightedQuickUnion {
private:
	int *id;
	int *numChildren; //Array that stores number of children for any given root
	int size;
public:
	WeightedQuickUnion() { size = 0; }
	WeightedQuickUnion(int s) {
		size = s;
		numChildren = new int[size];
		id = new int[size];
		for (int n = 0; n < size; n++) {
			id[n] = n;
			numChildren[n] = 1; //All roots contribute one to the size of their respective trees
		}
	}
	int getRoot(int a) {
		while (a != id[a])
			a = id[a];
		return a;
	}
	void unite(int a, int b) {
		if (numChildren[a] < numChildren[b]) {
			id[a] = b;
			numChildren[b] += numChildren[a];
		} else {
			id[b] = a;
			numChildren[a] += numChildren[b];
		}
	}
	void print() {
		std::cout << "Index   ";
		for (int n = 0; n < size; n++)
			std::cout << n << " ";
		std::cout << std::endl << "Content ";
		for (int n = 0; n < size; n++)
			std::cout << id[n] << " ";
		std::cout << std::endl << std::endl;
	}
	~WeightedQuickUnion() { delete[] id; delete[] numChildren; }
};

class WeightedQuickUnionHeight {
private:
	int *id;
	int size;
	int *depth; //Array that stores depth of a tree for any given root
public:
	WeightedQuickUnionHeight() { size = 0; }
	WeightedQuickUnionHeight(int s) {
		size = s;
		id = new int[size];
		depth = new int[size];
		for (int n = 0; n < size; n++) {
			id[n] = n;
			depth[n] = 1; //Initially, each root contributes 1 to the height of its tree
		}
	}
	int getRoot(int a) {
		while (a != id[a])
			a = id[a];
		return a;
	}
	void unite(int a, int b) {
		if (depth[getRoot(a)] < depth[getRoot(b)]) { //If tree containing node A is shorter than tree containing node B, connect A to B.
			id[getRoot(a)] = getRoot(b); //Root of A now points to root of B. Do not adjust heights because A does not make B any taller.
		} else { //If tree containing node B is shorter than tree containing node A OR a and b are of same height
			id[getRoot(b)] = getRoot(a);
			if (depth[getRoot(a)] == depth[getRoot(b)]) //If both heights were equal, B now has a leaf on the very bottom that increases its height by 1.
				depth[getRoot(b)] += 1;
		}	
	}
	void print() {
		std::cout << "Index   ";
		for (int n = 0; n < size; n++)
			std::cout << n << " ";
		std::cout << std::endl << "Content ";
		for (int n = 0; n < size; n++)
			std::cout << id[n] << " ";
		std::cout << std::endl << std::endl;
	}
	~WeightedQuickUnionHeight() { delete[] id; delete[] depth; }
};

int main(int argc, char* argv[]) {
	
	//Show contents of array after each union operation
	
	/*QuickFind qf(7);
	qf.unite(0, 2);
	qf.print();
	qf.unite(1, 4);
	qf.print();
	qf.unite(2, 5);
	qf.print();
	qf.unite(3, 6);
	qf.print();
	qf.unite(0, 4);
	qf.print();
	qf.unite(6, 0);
	qf.print();
	
	QuickUnion qu(7);
	qu.unite(0, 2);
	qu.print();
	qu.unite(1, 4);
	qu.print();
	qu.unite(2, 5);
	qu.print();
	qu.unite(3, 6);
	qu.print();
	qu.unite(0, 4);
	qu.print();
	qu.unite(6, 0);
	qu.print();
	qu.unite(1, 3);
	qu.print();
	
	WeightedQuickUnion wqu(7);
	wqu.unite(0, 2);
	wqu.print();
	wqu.unite(1, 4);
	wqu.print();
	wqu.unite(2, 5);
	wqu.print();
	wqu.unite(3, 6);
	wqu.print();
	wqu.unite(0, 4);
	wqu.print();
	wqu.unite(6, 0);
	wqu.print();
	wqu.unite(1, 3);
	wqu.print();*/
	
	WeightedQuickUnionHeight wquh(7);
	wquh.print();
	wquh.unite(0, 2);
	wquh.print();
	wquh.unite(1, 4);
	wquh.print();
	wquh.unite(2, 5);
	wquh.print();
	wquh.unite(3 ,6);
	wquh.print();
	wquh.unite(0, 4);
	wquh.print();
	wquh.unite(6, 0);
	wquh.print();
	wquh.unite(1, 3);
	wquh.print();
	
	return 0;
}
