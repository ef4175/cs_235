#ifndef QUEUE_H
#define QUEUE_H
#include "Node.h"

template <class T>
class Queue {
private:
    Node<T> *head;
    Node<T> *tail;
    int size;
public:
    Queue();
    void enqueue(Node<T>*);
    void enqueue(Node<T>);
    void enqueue(T*);
    void enqueue(T);
    Node<T> dequeue();
    int getSize();
    bool isEmpty();
    ~Queue();
};

#include "Queue.cpp"
#endif
