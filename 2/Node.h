#ifndef NODE_H
#define NODE_H

template <class T>
class Node {
private:
    T data;
    Node *next;
    Node *prev;
public:
    Node();
    Node(T);
    void setData(T);
    void setData(T*);
    void setPrev(Node*);
    void setPrev(Node);
    void setNext(Node*);
    void setNext(Node);
    T getData();
    void setNextToNull();
    void setPrevToNull();
    Node* getPrev();
    Node* getNext();
    ~Node();
};

#include "Node.cpp"
#endif
