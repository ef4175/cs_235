#include <iostream>
#include "Node.h"
#include "Stack.h"
#include "Queue.h"

int main(int argc, char* argv[]) {

    //Reverse order of a queue using a stack
    Queue<int> q;
    Stack<int> s;
    
    //Enqueue 8 integers (could be any number of objects of any data type)
    q.enqueue(100);
    q.enqueue(101);
    q.enqueue(102);
    q.enqueue(103);
    q.enqueue(104);
    q.enqueue(105);
    q.enqueue(106);
    q.enqueue(107);
    
    //Put these numbers into the stack
    for (int n = 0; n < 8; n++) {
        Node<int> m = q.dequeue();
        s.push(m);
    }
    //Now queue is empty, and stack has 8 integers
    //Latest item on stack contains earliest item in queue
    
    //Transfer elements back to queue
    for (int n = 0; n < 8; n++) {
        Node<int> m = s.pop();
        q.enqueue(m);
    }
    //Stack is now empty, and queue is reversed
    
    //A check for correctness of this program that destroys the queue.
    std::cout << "Reversed queue      ";
    for (int n = 0; n < 8; n++) {
        Node<int> m = q.dequeue();
        std::cout << m.getData() << " ";
    }
    std::cout << std::endl; 
    
    //------------------------------------------------------------------------
    
    //Get "valley" of entries in an array
    const int SIZE_ONE = 7;
    int prices[SIZE_ONE] = {100, 80, 60, 70, 60, 75, 85};
    /*Set all to zero. This eliminates the need to check if an element 
    is smaller than its predecessor, which implies that the valley of
    this element is zero.*/
    int valley[SIZE_ONE] = {0, 0, 0, 0, 0, 0, 0};
    Stack<int> displacement;
    int largest = prices[0];
    displacement.push(0);
    
    
    for (int n = 1; n < SIZE_ONE; n++) {
    
        if (prices[n] >= largest) {  //If element is largest seen thus far,
            valley[n] = n;           //then its valley is its index
            largest = prices[n];
        }
        
        /*If an element is greater than the one before it, then it must
        be compared with previous elements. These previous elements are
        obtained from the stack. Pop the stack while the value of the
        current element is greater than or equal to the popped element.
        This stops when current element is less than the one on top of
        the stack; valley of current element is equal to its index minus
        the index of the larger element minus one. It can be proved by
        contradiction that this eliminates superfluous checking for larger
        elements further down the array because we know that these larger
        elements are greater than or equal to the ones that were 
        popped off the stack*/
        
        else if (prices[n] >= prices[n - 1]) {
            while (prices[n] >= prices[displacement.top()])
                Node<int> garbage = displacement.pop();
            valley[n] = n - displacement.top() - 1;
        }
        
        displacement.push(n);
    }
    
    //A check for the correctness of the algorithm
    std::cout << "First valley array  ";
    for (int n = 0; n < SIZE_ONE; n++)
        std::cout << valley[n] << " ";
    std::cout << std::endl;
    
    //------------------------------------------------------------------------
    
    /*Another run of the algorithm. Note that the largest value appears
    somwhere in the middle of the array.*/
    const int SIZE_TWO = 16;
    double expenditures[SIZE_TWO] = {55.99, 63, 66.66, 69.69, 75, 1337.13,
    82.50, 90.95, 91, 96.50, 100, 105.25, 123.45, 133.70, 144.44, 191};
    int valley2[SIZE_TWO];
    for (int n = 0; n < SIZE_TWO; n++)
        valley2[n] = 0;
    Stack<int> disp;
    int greatest = expenditures[0];
    disp.push(0);
    
    for (int n = 1; n < SIZE_TWO; n++) {
        if (expenditures[n] >= greatest) {
            valley2[n] = n;
            greatest = expenditures[n];
        } else if (expenditures[n] >= expenditures[n - 1]) {
            while (expenditures[n] >= expenditures[disp.top()])
                Node<int> garbage = disp.pop();
            valley2[n] = n - disp.top() - 1;
        }
        disp.push(n);
    }
    
    std::cout << "Second valley array ";
    for (int n = 0; n < SIZE_TWO; n++)
        std::cout << valley2[n] << " ";
    std::cout << std::endl;
    
    return 0;
}
