template <class T>
Node<T>::Node(): prev(NULL), next(NULL) {}

template <class T>
Node<T>::Node(T d): prev(NULL), next(NULL), data(d) {}

template <class T>
void Node<T>::setData(T d) { data = d; }

template <class T>
void Node<T>::setData(T* d) { data = *d; }

template <class T>
void Node<T>::setPrev(Node<T>* n) { 
    this -> prev = n; 
    n -> next = this;
}

template <class T>
void Node<T>::setPrev(Node<T> n) {
    //Make new node so as not to modify a non-pointer variable
    Node<T> *newNode = new Node<T>;
    newNode -> data = n.data;
    this -> prev = newNode;
    newNode -> next = this;
}

template <class T>
void Node<T>::setNext(Node<T>* n) {
    this -> next = n;
    n -> prev = this;
}

template <class T>
void Node<T>::setNext(Node<T> n) {
    Node<T> *newNode = new Node<T>;
    newNode -> data = n.data;
    this -> next = newNode;
    newNode -> prev = this;
}

template <class T>
T Node<T>::getData() { return data; }

template <class T>
void Node<T>::setPrevToNull() { this -> prev = NULL; }

template <class T>
void Node<T>::setNextToNull() { this -> next = NULL; }

template <class T>
Node<T>* Node<T>::getPrev() { return prev; }

template <class T>
Node<T>* Node<T>::getNext() { return next; }

template <class T>
Node<T>::~Node() {}
