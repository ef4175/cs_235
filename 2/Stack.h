#ifndef STACK_H
#define STACK_H
#include "Node.h"

template <class T>
class Stack {
private:
    Node<T> *head;
    Node<T> *tail;
    int size;
public:
    Stack();
    void push(Node<T>*);
    void push(Node<T>);
    void push(T*);
    void push(T);
    Node<T> pop();
    T top();
    int getSize();
    bool isEmpty();
    ~Stack();
};

#include "Stack.cpp"
#endif
