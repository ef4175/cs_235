template <class T>
Stack<T>::Stack(): size(0) {
    head = new Node<T>();
    tail = new Node<T>();
}

template <class T>
void Stack<T>::push(Node<T>* n) {
    if (size == 0) {
        delete head;  //Free allocated memory and let head point to argument
        head = n; 
        head -> setPrevToNull(); //Can't be certain that n's pointer variables
        head -> setNextToNull(); //point to NULL
    } else if (size == 1) {
        delete tail;  //Free memory
        tail = n;
        tail -> setNextToNull();
        tail -> setPrev(head);  //head's next variable gets adjusted as 
    } else {                    //tail's prev variable does
        tail -> setNext(n);
        tail = n;
        tail -> setNextToNull();
    }
    size++;
}

template <class T>
void Stack<T>::push(Node<T> n) {
    Node<T> *newNode = new Node<T>(n.getData());
    if (size == 0) {
        delete head;
        head = newNode;
    } else if (size == 1) {
        delete tail;
        tail = newNode;
        tail -> setPrev(head);
    } else {
        tail -> setNext(newNode);
        tail = newNode;
        tail -> setNextToNull();
    }
    size++;
}

//push functions for non-node data types
template <class T>
void Stack<T>::push(T *o) {
    Node<T> *newNode = new Node<T>(*o);
    if (size == 0) {
        delete head;
        head = newNode;
    } else if (size == 1) {
        delete tail;
        tail = newNode;
        tail -> setPrev(head);
    } else {
        tail -> setNext(newNode);
        tail = newNode;
        tail -> setNextToNull();
    }
    size++;
}

template <class T>
void Stack<T>::push(T o) {
    Node<T> *newNode = new Node<T>(o);
    if (size == 0) {
        delete head;
        head = newNode;
    } else if (size == 1) {
        delete tail;
        tail = newNode;
        tail -> setPrev(head);
    } else {
        tail -> setNext(newNode);
        tail = newNode;
        tail -> setNextToNull();
    }
    size++;
}

template <class T>
Node<T> Stack<T>::pop() {
    Node<T> removed;
    if (size == 0) {}  //Empty function averts else clause for size == 0
    else if (size == 1) {  
        removed.setData(head -> getData());
        delete head;  //Delete useless head and create a new one
        head = new Node<T>();
        size--;
        return removed;
    } else if (size == 2) { //delete previous tail
        removed.setData(tail -> getData());
        delete tail;    
        tail = new Node<T>();   
        size--;
        return removed;
    } else {  
        removed.setData(tail -> getData());
        tail = tail -> getPrev();
        delete (tail -> getNext());
        tail -> setNextToNull();
        size--;
        return removed;
    }
}

template <class T>
T Stack<T>::top() {  //get data on top of stack, but do not pop
    if (size == 0) return 0;
    else if (size == 1) return head -> getData();
    return tail -> getData();
}

template <class T>
int Stack<T>::getSize() { return size; }

template <class T>
bool Stack<T>::isEmpty() { return size == 0; }

template <class T>
Stack<T>::~Stack() {
    if (size > 1) {
        while (head -> getNext() != NULL) {  //Delete element by element, but
            head = head -> getNext();        //does not delete tail
            delete head -> getPrev();
        }
    } else delete head;  //Delete head if size of list is 0 or 1
    delete tail;  //Delete tail in all cases
    head = NULL;
    tail = NULL;
}
