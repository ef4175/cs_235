template <class T>
Queue<T>::Queue(): size(0) {
    head = new Node<T>();
    tail = new Node<T>();
}

template <class T>
void Queue<T>::enqueue(Node<T>* n) {
    if (size == 0) {
        delete head;  //Free allocated memory and let head point to argument
        head = n;
        head -> setPrevToNull();  //Can't be certain that n's pointer
        head -> setNextToNull();  //variables point to NULL
    } else if (size == 1) {
        delete tail;  //Free memory and let tail point to argument
        tail = n;
        tail -> setNextToNull();
        tail -> setPrev(head);  //head's next variable gets adjusted
    } else {
        tail -> setNext(n);  //Link tail and next
        tail = n;            //new tail
        tail -> setNextToNull();
    }
    size++;
}

template <class T>
void Queue<T>::enqueue(Node<T> n) {
    Node<T> *newNode = new Node<T>(n.getData());
    if (size == 0) {
        delete head;  //Free memory; let head, newNode point to same location
        head = newNode;
    } else if (size == 1) {
        delete tail;  //Free memory; let tail, newNode point to same location
        tail = newNode;
        tail -> setPrev(head);
    } else {
        tail -> setNext(newNode);
        tail = newNode;
        tail -> setNextToNull();
    }
    size++;
}

//enqueue functions for non-node data types
template <class T>
void Queue<T>::enqueue(T* o) {
    Node<T> *newNode = new Node<T>(*o);
    if (size == 0) {
        delete head;
        head = newNode;
    } else if (size == 1) {
        delete tail;
        tail = newNode;
        tail -> setPrev(head);
    } else {
        tail -> setNext(newNode);
        tail = newNode;
        tail -> setNextToNull();
    }
    size++;
}

template <class T>
void Queue<T>::enqueue(T o) {
    Node<T> *newNode = new Node<T>(o);
    if (size == 0) {
        delete head;
        head = newNode;
    } else if (size == 1) {
        delete tail;
        tail = newNode;
        tail -> setPrev(head);
    } else {
        tail -> setNext(newNode);
        tail = newNode;
        tail -> setNextToNull();
    }
    size++;
}

template <class T>
Node<T> Queue<T>::dequeue() {
    Node<T> removed;  //Return node object with data only
    if (size == 0) {}  //Put empty function to avert else clause for size 0
    else if (size == 1) {
        removed.setData(head -> getData());
        delete head;           //delete useless head
        head = new Node<T>();  //allocate memory for a new head 
        size--;
        return removed;
    } else if (size == 2) {  //head becomes tail, then delete previous head
        removed.setData(head -> getData());
        delete head;
        head = tail;
        tail = new Node<T>();  //new node for tail; link head and tail
        head -> setPrevToNull();
        tail -> setPrev(head);
        size--;
        return removed;
    } else {  //head becomes second in line, then delete node preceding it
        removed.setData(head -> getData());
        head = head -> getNext();
        delete (head -> getPrev());
        head -> setPrevToNull();
        size--;
        return removed;
    }
}

template <class T>
int Queue<T>::getSize() { return size; }

template <class T>
bool Queue<T>::isEmpty() { return size == 0; }

template <class T>
Queue<T>::~Queue() {
    if (size > 1) {
        while (head -> getNext() != NULL) {  //Delete element by element, but
            head = head -> getNext();        //does not delete tail
            delete head -> getPrev();
        }
    } else delete head;  //Delete head if size of list is 0 or 1
    delete tail;  //Delete tail in all cases
    head = NULL;
    tail = NULL;
}
