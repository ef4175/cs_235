Implementation for a stack and queue for CS 235.

Quick notes about the classes:
-Linking two nodes requires that just one object call one of its functions.
For instance, if a and b are two nodes to be joined with a preceding b, then
a.setNext(b) or b.setPrev(a) will have the same effect.

-Node's destructor is empty because we deal with memory deallocation in
the Stack and Queue classes.

-There are four "appending" for each of the classes Stack and Queue. This is
to ensure that the majority of cases are covered.

-If any of their nodes are removed, Stack and Queue return a node rather than
data 



Check main.cpp for one possibility for question 2.
Check main.cpp for some sample runs for question 3.
